#!/bin/bash

now="$(date +'%Y-%m-%d')"

output="$now"_"$1"

command=$( cp "$1" $output )

echo "Input file : $1"
echo "Output file : $output"
$command
