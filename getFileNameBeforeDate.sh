#!/bin/bash

clean=$( basename $1 _$(date +'%Y-%m-%d').txt )
extension="${1#*.}"

command=$( cp $1 "$clean.$extension")

$command

